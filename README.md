# Java Programming Essentials
This repository is a storage of information about the Essential knowledge</br> 
for a QA Developer needed to write for both UI and API Automation in Java.
</br></br>

### Table of Contents
- Abstract Class
- Access Modifiers
- Control Structures
- Encapsulation
- Exception Handling
- File Handling
- Inheritance
- Interface
- My First Java Project
- Polymorphism

</br></br>

### Credits
[REST Assured API Automation + Framework: From Zero to Hero!](https://www.udemy.com/course/rest-assured-api-automation/) </br>
[By Omprakash Chavan](https://www.udemy.com/user/omprakash-chavan/)