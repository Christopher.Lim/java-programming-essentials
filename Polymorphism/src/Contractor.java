public class Contractor extends Employee {

    /* Override the method of salary from Employee*/
    @Override
    int salary() {
        return base + 10000;
    }

    /* Methods that uses static from the
    Super Class cannot be overwritten*/
    //@Override
    static String designation(){
        return "contractor";
    }
}
