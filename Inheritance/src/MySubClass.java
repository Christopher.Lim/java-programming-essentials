public class MySubClass extends MySuperClass{
    String commonStr = "sub common string";

    public MySubClass(String constructorStr){
        super(constructorStr);
        System.out.println("My SubClass Constructor");
    }

    public void subClassMethod(){
        System.out.println("My SubClass Method");
    }

    public void printCommonString(){
        System.out.println(commonStr);
        System.out.println(super.commonStr);
    }

    public void commonMethod(){
        System.out.println("sub common method");
    }

    public void printcommonMethod(){
        commonMethod();
        super.commonMethod();;
    }
}


